# Copyright (c) 2018, Tinghui Wang <tinghui.wang@wsu.edu>
# All rights reserved.

import os
from setuptools import setup
from setuptools import find_packages
from Cython.Build import cythonize


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
Intended Audience :: Science/Research
License :: OSI Approved :: BSD License
Proramming Language :: Python :: 3.6
Topic :: Scientific/Engineering :: Artificial Intelligence
Topic :: Scientific/Engineering :: Information Analysis
""".splitlines()

NAME = "pycasas"
MAINTAINER = "Tinghui Wang (Steve)"
MAINTAINER_EMAIL = "tinghui.wang@wsu.edu"
DESCRIPTION = "CASAS Smart Home Data Pre-processing Package."
LONG_DESCRIPTION = DESCRIPTION
LICENSE = "MIT"
URL = "https://gitlab.com/leavesw/pycasas"
AUTHOR = "Tinghui Wang (Steve)"
AUTHOR_EMAIL = "tinghui.wang@wsu.edu"

exec_results = {}
exec(
    open(
        os.path.join(os.path.dirname(__file__), 'pycasas/_version.py')
    ).read(),
    exec_results
)
version = exec_results['version']

with open(
        os.path.join(os.path.dirname(__file__), 'requirements.txt'), 'r'
) as f:
    install_requires = f.read().splitlines()


def do_setup():
    setup(
        name=NAME,
        version=version,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        classifiers=CLASSIFIERS,
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        url=URL,
        license=LICENSE,
        keywords=' '.join(['Smart Home', 'CASAS']),
        packages=find_packages('.'),
        install_requires=install_requires,
        ext_modules=cythonize('pycasas/data/*.pyx', gdb_debug=True)
    )


if __name__ == "__main__":
    do_setup()
